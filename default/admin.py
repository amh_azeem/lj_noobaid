from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import User

from default.models import Profile, Job, JobApplication

class JobAdmin(admin.ModelAdmin):
    list_display= ('title', 'description', 'location','hirer','updated_on')
class ApplicationAdmin(admin.ModelAdmin):
    list_display= ('user', 'job', 'resume','updated_on')
    actions = ['download_csv']

    def download_csv(self, request, queryset):
        import csv
        from django.http import HttpResponse

        f = open('some.csv', 'wb')
        writer = csv.writer(f)
        writer.writerow(["code", "country", "ip", "url", "count"])

        for s in queryset:
            writer.writerow([s.code, s.country, s.ip, s.url, s.count])

        f.close()

        f = open('some.csv', 'r')
        response = HttpResponse(f, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=stat-info.csv'
        return response

    download_csv.short_description = "Download CSV file for selected stats."

admin.site.register(Profile)
admin.site.register(Job,JobAdmin)
admin.site.register(JobApplication,ApplicationAdmin)
