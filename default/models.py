from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver

LEVELS=(
    ('E','Entry Level (0 - 2 yrs)'),
    ('M','Mid-level (3 - 4 yrs)'),
)

class Profile(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    instituition=models.CharField(max_length=354,null=True,default="")
    avatar=models.ImageField(upload_to='avatars')
    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance,)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()
    def __str__(self):
        return self.user.username+ " : " +self.user.first_name + self.user.last_name



class Job(models.Model):
    title=models.CharField(max_length=254)
    description=models.TextField()
    location=models.CharField(max_length=254,default='')
    job_descriptionlink=models.URLField(null=True,blank=True)
    hirer=models.CharField(max_length=254, default='Linuxjobber')
    level=models.CharField(max_length=2,default='E',choices=LEVELS)
    updated_on=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

class JobApplication(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    job=models.ForeignKey(Job,on_delete=models.CASCADE,null=True,blank=True)
    resume=models.FileField(upload_to='job_resumes')
    updated_on=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.job.title + ' ' + self.user.username
# Todo Design the model Appropriately



