/**
 * Convert a single file-input element into a 'multiple' input list
 *
 * Usage:
 *
 *   1. Create a file input element (no name)
 *      eg. <input type="file" id="first_file_element">
 *
 *   2. Create a DIV for the output to be written to
 *      eg. <div id="files_list"></div>
 *
 *   3. Instantiate a MultiSelector object, passing in the DIV and an (optional) maximum number of files
 *      eg. var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 3 );
 *
 *   4. Add the first element
 *      eg. multi_selector.addElement( document.getElementById( 'first_file_element' ) );
 *
 *   5. That's it.
 *
 *   You might (will) want to play around with the addListRow() method to make the output prettier.
 *
 *   You might also want to change the line 
 *       element.name = 'file_' + this.count;
 *   ...to a naming convention that makes more sense to you.
 * 
 * Licence:
 *   Use this however/wherever you like, just don't blame me if it breaks anything.
 *
 * Credit:
 *   If you're nice, you'll leave this bit:
 *  
 *   Class by Stickman -- http://www.the-stickman.com
 *      with thanks to:
 *      [for Safari fixes]
 *         Luis Torrefranca -- http://www.law.pitt.edu
 *         and
 *         Shawn Parker & John Pennypacker -- http://www.fuzzycoconut.com
 *      [for duplicate name bug]
 *         'neal'
 */
function MultiSelector(list_target, max) {
    // Where to write the list
    //console.log(list_target);
    var i = 0;
    this.list_target = list_target;
    this.count = 0;
    this.id = 0;
    var validExtensions = ["thm", "bmp", "tif", "jpg", "jpeg", "gif", "png", "doc", "docx", "txt", "pdf"];
    if (max) {
        this.max = max;
    } else {
        this.max = -1;
    }
    ;
    /**
     * Add a new file input element
     */
    this.addElement = function(element) {

        // Make sure it's a file input element
        if (element.tagName == 'INPUT' && element.type == 'file') {
            // Element name -- what number am I?
            //	element.name = 'file_' + this.id++;
            element.name = 'name[' + this.id++ + ']';

//            element.id = 'UserDirectory' + this.id++ + 'FileName';
            jQuery('#file-5').attr('id', 'problemPage' + this.id++ + 'FileName');
            jQuery(element).attr('id', 'file-5');
            element.className = 'multi-select-btn';
            // Add reference to this object
            element.multi_selector = this;
            // What to do when a file is selected onclick="return countFile();"
//            element.onclick = function (evt) {
//                if (jQuery('.InputfileName').length > 4) {
//                    evt.preventDefault();
//                    element.disabled = true;
//                    alert('You can upload maximum 5 files at a time.');
//                } 
//            };
            element.onchange = function() {
                // New file input
                var new_element = document.createElement('input');
                new_element.type = 'file';
                // Add new element
                this.parentNode.insertBefore(new_element, this);
                // Apply 'update' to element
                this.multi_selector.addElement(new_element);
                // Update list
                this.multi_selector.addListRow(this);
                // Hide this: we can't use display:none because Safari doesn't like it
                this.style.position = 'absolute';
                this.style.left = '-1000px';
                this.style.display = 'none';
            };

            // If we've reached maximum number, disable input element
            if (this.max != -1 && this.count >= this.max) {
                element.disabled = true;
            }
            ;
            // File element counter
            this.count++;
            // Most recent element
            this.current_element = element;
        } else {
            // This can only be applied to file input elements!
            alert('Error: not a file input element');
        }
        ;
    };
    /**
     * Remove All the Selected Files
     */
    this.removeSelectedFiles = function(element) {
        var files = element.files;
        var fileId = jQuery(element).attr('id');
        jQuery('#' + fileId).val();
        this.count = 0;
        jQuery('#' + fileId).parents('.upload-files').find('#files_list').html(' ');
        jQuery('#' + fileId).parents('.upload-files').find('.multi-select-btn').not('#file-5').remove();
        jQuery('#file-5').css('');
        jQuery('#reportForm').find('.dz-preview').remove();

    };
    /**
     * Add a new row to the list of files
     */
    this.addListRow = function(element) {
        // Row div
        var new_row = document.createElement('div');
        new_row.className = 'marginBottom';
        // Delete button
        var new_row_button = document.createElement('input');
        new_row_button.type = 'button';
        new_row_button.value = 'X';
        new_row_button.className = 'multiUploadFileDlttBtn';
        // References
        new_row.element = element;

        // Delete function
        new_row_button.onclick = function() {
            // Remove element from form
            this.parentNode.element.parentNode.removeChild(this.parentNode.element);

            // Remove this row from the list
            this.parentNode.parentNode.removeChild(this.parentNode);

            // Decrement counter
            this.parentNode.element.multi_selector.count--;

            // Re-enable input element (if it's disabled)
            this.parentNode.element.multi_selector.current_element.disabled = false;

            // Appease Safari
            //    without it Safari wants to reload the browser window
            //    which nixes your already queued uploads
            return false;
        };

        // Set row value
        //new_row.innerHTML = element.value;
        var filename = element.value.substr(element.value.lastIndexOf("\\") + 1);
        var len = filename.length;
        var ext = filename.split('.').pop();
        if (len > 15) {
            var prefix = filename.substr(0, 10);
            var sufix = filename.substr(len - 10);
            filename = prefix + '...' + sufix;
        }
        i++;
        if ($.inArray(ext, validExtensions) != -1) {
            if (ext == 'doc' || ext == 'docx') {
                var imgSrc = '/img/doc.png';
            } else if (ext == 'pdf') {
                var imgSrc = '/img/pdf.png';
            } else if (ext == 'txt') {
                var imgSrc = '/img/txt.png';
            } else {
                var imgSrc = '/img/image.png';
            }
            
            new_row.innerHTML = '<div class="attachImg filesinner' + i + '"><ul><li><img src=' + imgSrc + '></li></ul></div><input type="text" name="name[]" class="InputfileName" placeholder="' + filename + '" disabled="disabled">';
//            $('.filesinner' + (i + 1)).find('ul').append(html);
            // Add button
            new_row.appendChild(new_row_button);

            // Add it to the list
            this.list_target.appendChild(new_row);
        } else {
            alert(ext + " files are not allowed");
        }

    };

}
;